from selenium import webdriver
import time
from bs4 import BeautifulSoup
from fake_useragent import UserAgent
from selenium.webdriver.chrome.options import Options
import pickle
import re
import os

from fake_useragent import UserAgent
ua = UserAgent(verify_ssl=False)
ua.update()

i = 1
urls_specific = []
while i <= 3:
    opts = Options()
    opts.add_argument(
        "user-agent=" + str(ua.random))
    browser = webdriver.Chrome(options=opts)
    url = "https://www.kickstarter.com/discover/advanced?category_id=12&woe_id=0&sort=magic&seed=2722523&page=" +str(i)
    browser.get(url)
    continue
    html = browser.page_source
    soup = BeautifulSoup(html, 'html.parser')
    text = soup.prettify()
    img_placeholders = soup.find_all(attrs={'class': 'block img-placeholder w100p'})
    if len(img_placeholders) == 0:
        browser.close()
        time.sleep(5)
        continue
    for img_placeholder in img_placeholders:
        url_specific = re.findall('.*?href="(.*?)">', str(img_placeholder), re.S)[0].strip()
        urls_specific.append(url_specific)
    with open('filename.pickle', 'wb') as handle:
        pickle.dump(urls_specific, handle)
    print("page " + str(i) + " is done.")
    i += 1
    browser.close()

comment_urls =[url_specific+'/comments' for url_specific in urls_specific]

for url_specific in urls_specific:
    new_browser_1 = webdriver.Chrome()
    new_browser_1.get(url_specific)
    html_1 = new_browser_1.page_source
    # print(html_1)
    soup = BeautifulSoup(html_1, "html.parser")
    items = soup.find_all("<div", {'class':"Campaign-State-live"})
    # print(items)
    for item in items:
        project_name = item.find('h2', {'class':"type-28 type-24-md soft-black mb1 project-name"})
        head_intro = item.find('<p', {'class':"type-14 type-18-md soft-black project-description mb1"})
        money_pledged = item.find('<span', {'class':"ksr-green-500"})
        project_we_love = item.find('<span',{'class':"ml1"})
        author = item.find('div', {'class':"text-left type-16 bold clip text-ellipsis"})
        pledge_pledge = item.find('<span', {'class':"money"})
        location = item.find('<svg', {'class':"svg-icon__map-pin icon-20 fill-soft-black"})
        game_type = item.find('<span',{'class':"svg-icon__icon--compass icon-20 fill-soft-black"})
        back_num = item.find("<div", {'class':"block type-16 type-28-md bold dark-grey-500"})
        self_intro = item.find("<div", {'class':"text-left"})
        author_pro_num = item.find("<div", {'class':"text-left mb3"})
        author_back_num = item.find("<div",{'class':"divider"})
        days_to_go = item.find("<div",{'class':"ml5 ml0-lg"})
        deadline = item.find("span",{'data-test-id':"deadline-exists"})

    info=list(project_name,head_intro,money_pledged,project_we_love,author,pledge_pledge,location,game_type,back_num,self_intro,author_pro_num,author_back_num).lst

    import csv
    #  1.创建文件对象
    f = open('raw_data.csv', 'w', encoding='utf-8')
    #  2.基于文件对象构建csv写入对象
    csv_write = csv.writer(f)
    #  3.构建列表头
    csv_write.writerow(['project_name','head_intro','money_pledged','project_we_love','author','pledge_pledge','location','game_type','back_num','self_intro','author_pro_num','author_back_num'])
    #  4.写入csv文件
    csv_write.writerow([project_name,head_intro,money_pledged,project_we_love,author,pledge_pledge,location,game_type,back_num,self_intro,author_pro_num,author_back_num])
    #  5.关闭文件
    f.close()

#评论
for comment_url in comment_urls:
    new_browser_2 = webdriver.Chrome()
    new_browser_2.get(comment_url)
    html_2 = new_browser_2.page_source
    # print(html_2)
    soup = BeautifulSoup(html_2, "html.parser")
    for i in range(0,1,2,3,4,5,6,7):
        comment_dict = {}
        items = soup.find_all("<div",{'class' : "data-comment-text type-14 mb[i]"})
        comment_dict[comment_url]=str(items)
    with open('raw_comments_data.txt', 'w') as f:  # 设置文件对象
        f.write(comment_dict)  # 将字符串写入文件中