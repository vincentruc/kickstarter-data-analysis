from selenium import webdriver
import requests
from bs4 import BeautifulSoup

browser = webdriver.Chrome(
    executable_path='/Users/jr-doc/Desktop/kickstarter-data-analysis/venv/lib/python3.10/site-packages/notebook/tests/selenium/chromedriver')
url = "https://www.kickstarter.com/projects/keyitechnology/meet-loona-the-petbot-you-will-fall-in-love?ref=recommendation-no-result-discoverpage-1"
browser.get(url)
html = browser.page_source
#print(html)
soup = BeautifulSoup(html, "html.parser")
items = soup.find_all("<div", class_='Campaign-State-live')
#print(items)
for item in items:
    project_name = item.find('h2', class_='type-28 type-24-md soft-black mb1 project-name').text
    head_intro = item.find('<p', class_="type-14 type-18-md soft-black project-description mb1").text
    money_pledged = item.find('<span', class_="ksr-green-500").text
    project_we_love = item.find('<span', class_='ml1').text
    author = item.find('div', class_="text-left type-16 bold clip text-ellipsis").text
    pledge_pledge = item.find('<span', class_='money').text
    location = item.find('<svg' , class_="svg-icon__map-pin icon-20 fill-soft-black").text
    game_type = item.find('<span', class_="svg-icon__icon--compass icon-20 fill-soft-black").text
    back_num = item.find("<div", class_="block type-16 type-28-md bold dark-grey-500")
    #comments_amount = item.find("span", class_="count")
    self_intro = item.find("<div", class_="text-left")
    author_pro_num = item.find("<div", class_='text-left mb3')
    author_back_num = item.find("<div", class_="divider")

