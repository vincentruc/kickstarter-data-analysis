# -*- coding:utf-8 -*-
from selenium import webdriver
from bs4 import BeautifulSoup
import re
import pandas as pd

from fake_useragent import UserAgent
from selenium.webdriver.chrome.options import Options


ua = UserAgent(verify_ssl=False)
ua.update()
opts = Options()
opts.add_argument("user-agent=" + str(ua.random))
driver = webdriver.Chrome(r"/Users/jr-doc/Desktop/kickstarter-data-analysis/venv/lib/python3.10/site-packages/notebook/tests/selenium/chromedriver", options = opts)
lis = ["analytics_name", "country", "goal", "usd_pledged", "percent_funded", "backers_count", "is_starrable", "staff_pick", "launched_at", "deadline"]
df = pd.DataFrame(columns = ['data-pid', 'url'] + lis)

def select(str, target):
	res = re.findall("\"" + target + "\":[\"0-9a-zA-Z./:\s-]*[,}]", str)[0]
	return res[len(target) + 3 :].rstrip("}").rstrip(",").strip("\"")

f = open(r'/Users/jr-doc/Desktop/kickstarter-data-analysis/data_related/data_processing/urls_specific.txt','r')
a = list(f)
for url_specific in a:
	n_url_specific = str(url_specific).strip('/n')
	driver.get(n_url_specific)
	text = driver.page_source
	text.encode('UTF-8')
	soup = BeautifulSoup(text, "lxml")
	item1 = soup.find_all('div', class_="js-react-proj-card grid-col-12 grid-col-6-sm grid-col-4-lg")
	for item in item1:
		if (item.has_attr("data-pid")):
			str = item["data-project"]
			newrow = [item["data-pid"], select(str, "project")]
			for info in lis:
				newrow.append(select(str, info))
			df.loc[len(df.index)] = newrow
df.to_csv("res.csv")