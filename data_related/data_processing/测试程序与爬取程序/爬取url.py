from selenium import webdriver
import time
from bs4 import BeautifulSoup
from fake_useragent import UserAgent
from selenium.webdriver.chrome.options import Options
import pickle
import re
from fake_useragent import UserAgent
ua = UserAgent(verify_ssl=False)
ua.update()

urls_specific = []
i = 71 #断电爬取，只需更改文件名
while i <= 100:
    opts = Options()
    opts.add_argument(
        "user-agent=" + str(ua.random))
    browser = webdriver.Chrome('/Users/jr-doc/Desktop/kickstarter-data-analysis/venv/lib/python3.10/site-packages/notebook/tests/selenium/chromedriver',options=opts)
    url = "https://www.kickstarter.com/discover/advanced?category_id=12&woe_id=0&sort=magic&seed=2722523&page=" +str(i)
    browser.get(url)
    html = browser.page_source
    soup = BeautifulSoup(html, 'html.parser')
    text = soup.prettify()
    img_placeholders = soup.find_all(attrs={'class': 'block img-placeholder w100p'})
    if len(img_placeholders) == 0:
        browser.close()
        time.sleep(5)
        continue
    for img_placeholder in img_placeholders:
        url_specific = re.findall('.*?href="(.*?)">', str(img_placeholder), re.S)[0].strip()
        urls_specific.append(url_specific)
    with open('filename.pickle', 'wb') as handle:
        pickle.dump(urls_specific, handle)
    print("page " + str(i) + " is done.")
    i += 1
    browser.close()
with open("filename.pickle", "rb") as f:
    obj = pickle.load(f)
    print(obj)
